This is the Debian GNU/Linux jags package. Jags is 'Just Another Gibbs
Sampler', a program for analysis of Bayesian Graphical models by Gibbs
Sampling.  Jags was written by Martyn Plummer.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the sources
	http://www-fis.iarc.fr/~martyn/software/jags/

Copyright and License information:

1) JAGS itself:
Copyright (C) 2002 - 2008 Martyn Plummer
License: GPL

2) The libltdl/ directory containing GNU libltdl (a system independent
dlopen wrapper for GNU libtool):
Copyright (C) 1998 - 2000, 2004 - 2007 Free Software Foundation, Inc.
Originally by Thomas Tanner <tanner@ffii.org>
License: LGPL

3) The src/jrmath/ directory containing a modified interface to GNU R 
(where Martyn Plummer is a member of the R Core Development Team)
Copyright (C) 1995, 1996 Robert Gentleman and Ross Ihaka
Copyright (C) 1998-2004 Ross Ihaka and the R Development Core Team
Copyright (C) 1998-2004 The R Development Core Team
Copyright (C) 1998--2005 The R Development Core Team
Copyright (C) 1998 Ross Ihaka
Copyright (C) 1999-2000 The R Development Core Team
Copyright (C) 2000-2001 The R Core Development Team
Copyright (C) 2000-2002 The R Development Core Team
Copyright (C) 2000, 2003 The R Development Core Team
Copyright (C) 2000-2005 The R Development Core Team
Copyright (C) 2000-2006 The R Development Core Team
Copyright (C) 2000-2006 The R Development Core Team
Copyright (C) 2000-2006 The R Development Core Team
Copyright (C) 2000 The R Core Development Team
Copyright (C) 2000 The R Development Core Team
Copyright (C) 2002-2004 The R Foundation
Copyright (C) 2003-2004 The R Foundation
Copyright (C) 2003-2004 The R Foundation
Copyright (C) 2003 The R Foundation
Copyright (C) 2004 Morten Welinder
Copyright (C) 2004 The R Foundation
Copyright (C) 2004 The R Foundation
Copyright (C) 2005-6 Morten Welinder <terra@gnome.org>
Copyright (C) 2005-6 The R Foundation
Copyright (C) 2005 The R Foundation
Copyright (C) 2006 The R Core Development Team
Copyright (C) 2006 The R Development Core Team
License: GPL

4) For the src/modules/base/rngs/MersenneTwisterRNG.cc
Copyright (C) 1997, 1999 Makoto Matsumoto and Takuji Nishimura.
License: GPL


On a Debian GNU/Linux system, the GPL and LGPL licenses are included
in the files /usr/share/common-licenses/GPL and
/usr/share/common-licenses/LGPL.

